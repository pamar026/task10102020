import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CommonService {

  showOverview = new BehaviorSubject(false)
  
  constructor() { }
}
