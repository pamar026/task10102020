import { Component, OnInit } from '@angular/core';
import { CommonService } from 'src/app/common.service';

@Component({
  selector: 'app-second-sidebar',
  templateUrl: './second-sidebar.component.html',
  styleUrls: ['./second-sidebar.component.css']
})
export class SecondSidebarComponent implements OnInit {
  sidebarLabel = ['Overview','Plugins','Themes']
  constructor(private commonService: CommonService) { }

  ngOnInit(): void {
  }

  openOverview(value){
    if(value === 'Plugins'){
      this.commonService.showOverview.next(true)  
    }
  }
}
