import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedRoutingModule } from './shared-routing.module';
import { HeaderComponent } from './header/header.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatSelectModule} from '@angular/material/select';
import {MatSidenavModule} from '@angular/material/sidenav';
import { RouterModule } from '@angular/router';
import { SecondSidebarComponent } from './second-sidebar/second-sidebar.component';



@NgModule({
  declarations: [HeaderComponent, SidebarComponent, SecondSidebarComponent],
  imports: [
    CommonModule,
    SharedRoutingModule,
    MatToolbarModule,
    MatSelectModule,
    MatSidenavModule,
    RouterModule
  ],
  exports:[
    HeaderComponent,
    SidebarComponent,
    SecondSidebarComponent
  ],
})
export class SharedModule {
  
 }
