import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  users = [
  
    { value: 'Profile', viewValue: 'Profile' },
    { value: 'Logout', viewValue: 'Logout' },
  ]

  headerArray = [
    {
      label: "Logo",
      route: "",
    },
    {
      label: "Dashboard",
      route: "/dashboard",
    },
    {
      label: "Orders",
      route: "",
      
    },
  ]

 
  constructor() { }

  ngOnInit(): void {
  }

}
