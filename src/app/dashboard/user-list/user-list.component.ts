import { Component, OnInit } from '@angular/core';
export interface PeriodicElement {
  name: string;
  position: number;
  email: string;
  action: string;
}

const ELEMENT_DATA: PeriodicElement[] = [
  {position: 1, name: 'Amar', email: 'amar@gmail.com', action: 'Edit'},
  {position: 2, name: 'Ram', email: 'ram@gmail.com', action: 'Edit'},
  {position: 3, name: 'Sam', email: 'sam@gmail.com', action: 'Edit'},
  {position: 4, name: 'Curren', email: 'curr@gmail.com', action: 'Edit'},
  {position: 5, name: 'Rahul', email: 'rahul@gmail.com', action: 'Edit'},
  {position: 6, name: 'Raju', email: 'raju@gmail.com', action: 'Edit'},
];

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})



export class UserListComponent implements OnInit {
  displayedColumns: string[] = ['position', 'name', 'email', 'action'];
  dataSource = ELEMENT_DATA;
  constructor() { }

  ngOnInit(): void {
  }

}
