import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DashboardRoutingModule } from './dashboard-routing.module';
import { SharedModule } from '../shared/shared.module';
import { UserListComponent } from './user-list/user-list.component';
import { UserDetailsComponent } from './user-details/user-details.component';
import {MatTableModule} from '@angular/material/table';
import {MatButtonModule} from '@angular/material/button';
import {MatDialogModule} from '@angular/material/dialog';
import { AddPluginDialogComponent } from './add-plugin-dialog/add-plugin-dialog.component';

@NgModule({
  declarations: [UserListComponent, UserDetailsComponent, AddPluginDialogComponent],
  imports: [
    CommonModule,
    DashboardRoutingModule,
    SharedModule,
    MatTableModule,
    MatButtonModule,
    MatDialogModule
  ],
  entryComponents: [ AddPluginDialogComponent],

})
export class DashboardModule { }
