import { Component, OnInit } from '@angular/core';
import { AddPluginDialogComponent } from '../add-plugin-dialog/add-plugin-dialog.component';
import { MatDialog } from '@angular/material/dialog';
import { CommonService } from 'src/app/common.service';

@Component({
  selector: 'app-user-details',
  templateUrl: './user-details.component.html',
  styleUrls: ['./user-details.component.css']
})
export class UserDetailsComponent implements OnInit {
  showOverview

  constructor(private dialog: MatDialog, private commonService : CommonService) { 
    this.commonService.showOverview
    .subscribe(
      (data)=>{
        this.showOverview = data
      }
    )
  }

  ngOnInit(): void {
  }

  openPluginDialog() {
    const dialogRef = this.dialog.open(AddPluginDialogComponent,{
      data:{
        
        buttonText: {
          cancel: 'Add'
        }
      },
    });
  }

}
