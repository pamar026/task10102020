import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddPluginDialogComponent } from './add-plugin-dialog.component';

describe('AddPluginDialogComponent', () => {
  let component: AddPluginDialogComponent;
  let fixture: ComponentFixture<AddPluginDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddPluginDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddPluginDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
